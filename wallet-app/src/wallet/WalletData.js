import React, { useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Table from 'react-bootstrap/esm/Table';
import bigDecimal, { RoundingModes } from 'js-big-decimal';
import QRCode from 'react-qr-code';
import SendMoney from './SendMoney';

export default function WalletData({ balance, mainAddr, keys }) {
  const [showAddrs, setShowAddrs] = useState(false);
  const [showQR, setShowQR] = useState(false);

  const copy = (value) => {
    navigator.clipboard.writeText(value);
    document.getElementById("copy").classList.remove("hidden");
    setTimeout(() => {
      document.getElementById("copy").classList.add("hidden");
    }, 1000)
  }

  var val = {confirmed: new bigDecimal(balance.confirmed).divide(new bigDecimal(100), 2), unconfirmed: new bigDecimal(balance.unconfirmed).divide(new bigDecimal(100), 2)}
  if (val.unconfirmed.compareTo(new bigDecimal(0)) < 0) {
    val.confirmed = val.confirmed.add(val.unconfirmed);
    val.unconfirmed = new bigDecimal(0);
  }
  var rAddrs = keys?.receiveKeys?.map(key => key.address).reverse();

  return (
    <>
      <Card.Title>Available</Card.Title>
      <Card.Title>{val.confirmed.round(2, RoundingModes.HALF_DOWN).getPrettyValue()} NEX</Card.Title>
      <div className="my-3">
        Pending
        <div>{val.unconfirmed.round(2, RoundingModes.HALF_DOWN).getPrettyValue()} NEX</div>
      </div>
      <div className="my-3">
        Receiving Address
        <i className="mx-1 fa-solid fa-qrcode cursor" title='QR code' onClick={() => setShowQR(true)}/>
        <div>                    
          <span className='text-monospace nx'>{mainAddr}</span>
          <i className="fa-regular fa-copy ms-1 cursor" aria-hidden="true" title='copy' onClick={() => copy(mainAddr)}/>
          <i id="copy" className="mx-1 fa fa-check nx hidden"/>
        </div>
      </div>
      <Button variant="outline-warning" onClick={() => setShowAddrs(true)}>My Addresses</Button>
      <SendMoney balance={balance} keys={keys}/>

      <Modal show={showAddrs} onHide={() => setShowAddrs(false)} backdrop="static" keyboard={false} aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title>Receive Addresses</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table borderless responsive striped>
            <tbody>
              {rAddrs?.map((addr, i) => <tr key={i}><td key={i}>{addr}</td></tr>)}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => setShowAddrs(false)}>Ok</Button>
        </Modal.Footer>
      </Modal>

      <Modal size='sm' show={showQR} onHide={() => setShowQR(false)} aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title>Address QR Code</Modal.Title>
        </Modal.Header>
        <Modal.Body className='center'>
          <QRCode value={mainAddr} size={200}/>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => setShowQR(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
