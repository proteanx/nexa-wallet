import React, { useRef, useState } from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/esm/Col';
import Row from 'react-bootstrap/esm/Row';
import { decryptMnemonic, getEncryptedSeed, isMnemonicValid } from '../utils/functions';

export default function WalletInfo({ height, scanAddresses }) {
  const [width] = useState(window.innerWidth);
  const isMobile = (width <= 768);

  const [showPwSeed, setShowPwSeed] = useState(false);
  const [showPw, setShowPw] = useState(false);
  const [showSeed, setShowSeed] = useState(false);
  const [seedContent, setSeedContent] = useState("");
  const [pwErr, setPwErr] = useState("");
  const [scanMsg, setScanMsg] = useState("");

  const pwRef = useRef("");

  const showBackup = () => {
    if (pwRef.current.value) {
      try {
        var encSeed = getEncryptedSeed();
        var decMn = decryptMnemonic(encSeed, pwRef.current.value);
        if (decMn && isMnemonicValid(decMn)) {
          setSeedContent(buildSeed(decMn.split(" ")));
          setShowPwSeed(false);
          setPwErr("");
          setShowSeed(true);
        } else {
          setPwErr("Incorrect password.")
        }
      } catch {
        setPwErr("Incorrect password.")
      }
    }
  }

  function buildSeed(words) {
    let content = [], columns = [];
    words.forEach ((word, i) => {
      columns.push(
        <Col key ={i}>{i+1}<div>{word}</div></Col>
      );
        
      if((i+1) % (isMobile ? 3 : 4) === 0) {
        content.push(<Row key={i} className='py-2'>{columns}</Row>);
        columns = [];
      }
    });
    return content;
  }

  const scanMoreAddresses = () => {
    setScanMsg("Scanning the next 10 addresses...");
    scanAddresses().then(res => {
      if (res) {
        setScanMsg("Found addresses. the data will be loaded soon.");
      } else {
        setScanMsg("No results.");
      }
      setTimeout(() => {
        setScanMsg("");
      }, 3000)
    }).catch(_ => {
      setScanMsg("Scan failed, please try again later and make sure the wallet is online.");
      setTimeout(() => {
        setScanMsg("");
      }, 3000)
    })
  }

  return (
    <div className='px-4'>
      <Table borderless className='text-white'>
        <tbody>
          <tr>
            <td className={isMobile || 'td-fit'}>Wallet Status</td>
            <td>{height ? "Online" : "Offline"}</td>
          </tr>
          <tr>
            <td>Version</td>
            <td>1.1.0</td>
          </tr>
          <tr>
            <td>Network</td>
            <td>nexa</td>
          </tr>
          <tr>
            <td>Block Height</td>
            <td>{height}</td>
          </tr>
        </tbody>
      </Table>
      <Button className='mx-2 mt-4' variant="outline-warning" onClick={() => setShowPwSeed(true)}>Backup Seed</Button>
      <Button className='mx-2 mt-4' variant="outline-warning" onClick={scanMoreAddresses} disabled={scanMsg != ''}>Scan More Addresses</Button>
      <div>
        {scanMsg}
      </div>

      <Modal show={showPwSeed} onHide={() => setShowPwSeed(false)} backdrop="static" keyboard={false} aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton={true}>
          <Modal.Title>Backup Seed</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Enter your password to decrypt the wallet</p>
          <InputGroup>
            <Form.Control type={!showPw ? "password" : "text"} ref={pwRef} placeholder="Password" autoFocus/>
            <InputGroup.Text className='cursor' onClick={() => setShowPw(!showPw)}>{!showPw ? <i className="fa fa-eye" aria-hidden="true"></i> : <i className="fa fa-eye-slash" aria-hidden="true"></i>}</InputGroup.Text>
          </InputGroup>
          <span className='bad'>
            {pwErr}
          </span>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowPwSeed(false)}>Cancel</Button>
          <Button variant="warning" onClick={showBackup}>Next</Button>
        </Modal.Footer>
      </Modal>

      <Modal className='center' show={showSeed} onHide={() => setShowSeed(false)} backdrop="static" keyboard={false} aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton={true}>
          <Modal.Title>Backup Seed</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {seedContent}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => setShowSeed(false)}>Ok</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
