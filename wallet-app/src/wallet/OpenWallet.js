import React, { useRef, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import CreateWallet from './create/CreateWallet';
import RecoverWallet from './recover/RecoverWallet';
import { decryptMnemonic, isMnemonicValid } from '../utils/functions';

export default function OpenWallet({ encSeed, setDecSeed }) {
  const [width] = useState(window.innerWidth);
  const isMobile = (width <= 768);

  const [makeNew, setMakeNew] = useState(false)
  const [recover, setRecover] = useState(false)
  const [pwErr, setPwErr] = useState("")
  const [showPw, setShowPw] = useState(false)

  const pwRef = useRef("");

  const open = () => {
    if (pwRef.current.value) {
      try {
        var decMn = decryptMnemonic(encSeed, pwRef.current.value);
        if (decMn && isMnemonicValid(decMn)) {
          setDecSeed(decMn);
        } else {
          setPwErr("Incorrect password.")
        }
      } catch {
        setPwErr("Incorrect password.")
      }
    }
  }

  const keyDown = (event) => {
    if(event.key === 'Enter'){
      event.preventDefault()
      open();
    }
  }

  const show = () => setShowPw(!showPw);
  const newWallet = () => setMakeNew(true);
  const recoverWallet = () => setRecover(true);
  const cancelNewWallet = () => setMakeNew(false);
  const cancelRecoverWallet = () => setRecover(false);

  if (makeNew) {
    return (
      <CreateWallet cancelCreate={cancelNewWallet} setDecSeed={setDecSeed}></CreateWallet>
    )
  }

  if (recover) {
    return (
      <RecoverWallet cancelRecover={cancelRecoverWallet} setDecSeed={setDecSeed}></RecoverWallet>
    )
  }

  return (
    <>
      <Card.Title className='pt-3'>Open Wallet</Card.Title>
      <hr/>
      <Form className='px-3 pb-3'>
        <Form.Group className="mb-2">
          <Form.Label>Unlock with your password</Form.Label>
          <InputGroup>
            <Form.Control type={!showPw ? "password" : "text"} ref={pwRef} placeholder="Password" autoFocus onKeyDown={keyDown}/>
            <InputGroup.Text className='cursor' onClick={show}>{!showPw ? <i className="fa fa-eye" aria-hidden="true"></i> : <i className="fa fa-eye-slash" aria-hidden="true"></i>}</InputGroup.Text>
            {isMobile && <Button variant="warning" onClick={open}>Open</Button>}
          </InputGroup>
          <Form.Text className="text-red">
            {pwErr}
          </Form.Text>
        </Form.Group>

        <div className='mt-4'>
          <Button variant="outline-warning" className='mx-2' onClick={recoverWallet}>Recover from Seed</Button>
          <Button variant="outline-warning" className='mx-2' onClick={newWallet}>New Wallet</Button>
          {isMobile || <Button variant="warning" className='mx-2' onClick={open}>Open Wallet</Button>}
        </div>
      </Form>
    </>
  )
}
