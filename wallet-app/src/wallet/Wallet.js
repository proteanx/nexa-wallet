import React, { useEffect, useState } from 'react'
import Bitcore from 'bitcore-lib-nexa'
import * as Bip39 from 'bip39';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import WalletInfo from './WalletInfo';
import WalletData from './WalletData';
import TxList from '../tx/TxList';
import { checkAddresses, checkBalanceAndTransactions, generateKeysAndAddresses, getBlockHeight } from '../utils/functions';
import bigDecimal from 'js-big-decimal';
import NexaAddress from '../utils/NexaAddress';
import { getLocalTransactions, addLocalTransactions, getAddressesIndexes, setAddressesIndexes, getTransactionsState, setTransactionsState, setLock, removeLock, getLastCheck, setLastCheck } from '../utils/localdb';
import WalletLoader from './WalletLoader';

export default function Wallet({ mnemonic, passphrase }) {
  const [width] = useState(window.innerWidth);
  const [keys, setKeys] = useState({});
  const [mainAddress, setMainAddress] = useState("");
  const [balance, setBalance] = useState({confirmed: 0, unconfirmed: 0});
  const [txState, setTxState] = useState({pending: 0, confirmed: 0, height: 0, total: 0});
  const [height, setHeight] = useState("");
  const [blocker, setBlocker] = useState(true);

  const isMobile = (width <= 768);

  const seed = Bip39.mnemonicToSeedSync(mnemonic, passphrase);
  const masterKey = Bitcore.HDPrivateKey.fromSeed(seed);
  const accountKey = masterKey.deriveChild(44, true).deriveChild(29223, true).deriveChild(0, true);
  
  useEffect(() => {
    removeLock("refreshing");
    getBlockHeight().then(res => {
      setHeight(res.toLocaleString());
    });
    loadCachedWallet().then(res => {
      if (res) {
        setBlocker(false);
        var lastChecked = getLastCheck();
        if (lastChecked == null || parseInt(lastChecked) < (Math.floor(Date.now() / 1000) - 90)) {
            refreshWallet();
        }
      } else {
        refreshWallet();
      }
    });
  }, []);
  
  console.log("rendering")
  async function loadCachedWallet() {
    var idx = getAddressesIndexes();
    if (idx.rIndex === 0) {
      return false;
    }

    console.log("load wallet from cache");
    var myKeys = generateKeysAndAddresses(accountKey, 0, idx.rIndex, 0, idx.cIndex);

    setKeys(myKeys);
    setMainAddress(myKeys.receiveKeys[myKeys.receiveKeys.length-1].address);

    var allTransactions = await getLocalTransactions();
    var transactionsState = getTransactionsState();
    setTxState(transactionsState);

    if (allTransactions.length > 0) {
      var available = new bigDecimal(0);
      var pending = new bigDecimal(0);
      for (let i = 0; i < allTransactions.length; i++) {
        if (allTransactions[i].confirmed) {
          available = available.add(new bigDecimal(allTransactions[i].value));
        } else {
          pending = pending.add(new bigDecimal(allTransactions[i].value));
        }
      }
      setBalance({confirmed: available.multiply(new bigDecimal(100)).getValue(), unconfirmed: pending.multiply(new bigDecimal(100)).getValue()});
    }
    return true;
  }

  function refreshWallet() {
    if (!setLock("refreshing")) {
      return; // already running
    }
    var idx = getAddressesIndexes();
    if (idx.rIndex === 0) {
      // first wallet creation / recovery
      idx = {rIndex: 15, cIndex: 15};
    }

    var myKeys = generateKeysAndAddresses(accountKey, 0, idx.rIndex, 0, idx.cIndex);

    var rAddrs = myKeys.receiveKeys.map(k => ({address: k.address, hex: NexaAddress.getAddressScript(k.address).toString('hex')}));
    var cAddrs = myKeys.changeKeys.map(k => ({address: k.address, hex: NexaAddress.getAddressScript(k.address).toString('hex')}));
    checkBalanceAndTransactions(rAddrs, cAddrs, getTransactionsState().height).then(res => {
      setLastCheck();
      if (res.receiveIndex !== idx.rIndex || res.changeIndex !== idx.cIndex) {
        setAddressesIndexes({rIndex: res.receiveIndex, cIndex: res.changeIndex});
        myKeys = generateKeysAndAddresses(accountKey, 0, res.receiveIndex, 0, res.changeIndex);
      }
      if (!keys.receiveKeys || keys.receiveKeys.length !== myKeys.receiveKeys.length || keys.changeKeys.length !== myKeys.changeKeys.length) {
        setKeys(myKeys);
      }
      if (mainAddress !== myKeys.receiveKeys[myKeys.receiveKeys.length-1].address) {
        setMainAddress(myKeys.receiveKeys[myKeys.receiveKeys.length-1].address)
      }
      if (balance.confirmed != res.balance.confirmed || balance.unconfirmed != res.balance.unconfirmed) {
        setBalance(res.balance);
      }
      if (txState.height !== res.transactions.height || txState.total !== res.transactions.total || txState.confirmed !== res.transactions.confirmed || txState.pending !== res.transactions.pending) {
        addLocalTransactions(res.transactions.txs, res.transactions.pending, res.transactions.confirmed, res.transactions.height, res.transactions.total).then(_ => {
          setTxState({pending: res.transactions.pending, confirmed: res.transactions.confirmed, height: res.transactions.height, total: res.transactions.total});
        });
      }
      if (res.height && height != res.height) {
        setHeight(res.height.toLocaleString());
      }
      if (blocker) {
        setBlocker(false);
      }
    }).catch(e => {
      setHeight("");
      console.log(e);
    }).finally(() => {
      removeLock("refreshing");
    });
  }

  async function scanAddresses() {
    var idx = getAddressesIndexes();
    var myKeys = generateKeysAndAddresses(accountKey, idx.rIndex, idx.rIndex + 10, idx.cIndex, idx.cIndex + 10);
    var rAddrs = myKeys.receiveKeys.map(k => ({address: k.address, hex: NexaAddress.getAddressScript(k.address).toString('hex')}));
    var cAddrs = myKeys.changeKeys.map(k => ({address: k.address, hex: NexaAddress.getAddressScript(k.address).toString('hex')}));

    var scanResult = await checkAddresses(rAddrs, cAddrs);
    var state = getTransactionsState();
    var indexes = getAddressesIndexes();
    if (scanResult.minHeight > 0) {
      if (scanResult.minHeight < state.height) {
        state.height = scanResult.minHeight;
        setTransactionsState(txState);
      }
      if (scanResult.receiveIndex > 0 || scanResult.changeIndex > 0) {
        setAddressesIndexes({rIndex: indexes.rIndex + scanResult.receiveIndex, cIndex: indexes.cIndex + scanResult.changeIndex});
      }
      return true;
    }
    return false;
  }

  if (blocker) {
    return (
      <WalletLoader isMobile={isMobile}/>
    )
  }

  return (
    <>
      <Card bg="custom-card" className='text-white'>
        <Card.Body>
          <Row>
            <Col className='center' style={{minWidth: "50%"}}>
              <WalletData balance={balance} mainAddr={mainAddress} keys={keys}/>
            </Col>
            { !isMobile ? (
            <Col className='vl' style={{minWidth: "50%"}}>
              <WalletInfo height={height} scanAddresses={scanAddresses}/>
            </Col>
            ) : ''}
          </Row>
          { isMobile ? (
          <Row className='mt-3 center'>
            <Col style={{minWidth: "50%"}}>
              <WalletInfo height={height} scanAddresses={scanAddresses}/>
            </Col>
          </Row>
          ) : ''}
        </Card.Body>
      </Card>
      <TxList refresh={refreshWallet} txCount={txState.total}/>
    </>
  )
}
