import React from 'react';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Placeholder from 'react-bootstrap/Placeholder';
import Table from 'react-bootstrap/Table';

export default function WalletLoader({ isMobile }) {
  return (
    <>
      <Card bg="custom-card" className='text-white'>
        <Card.Body>
          <Row>
            <Col className='center' style={{minWidth: "50%"}}>
              <Card.Title>Available</Card.Title>
              <Placeholder as={Card.Title} animation="glow">
                <Placeholder xs={5} size="lg"/>
              </Placeholder>
              <div className="my-3">
                Pending
                <div>
                  <Placeholder as={Card.Text} animation="glow">
                    <Placeholder xs={5}/>
                  </Placeholder>
                </div>
              </div>
              <div className="my-3">
                Receiving Address
                <div>                    
                  <Placeholder as={Card.Text} animation="glow">
                    <Placeholder xs={8} size="lg"/>
                  </Placeholder>
                </div>
              </div>
              <Placeholder as={Card.Text} animation="glow">
                <Placeholder.Button variant="warning" xs={2} />
              </Placeholder>
            </Col>
            { !isMobile ? (
            <Col className='vl' style={{minWidth: "50%"}}>
              <div className='px-4'>
                <Table borderless className='text-white'>
                  <tbody>
                    <tr>
                      <td className='td-fit'>Wallet Status</td>
                      <td>
                        <Placeholder as={Card.Text} animation="glow">
                          <Placeholder xs={4}/>
                        </Placeholder>
                      </td>
                    </tr>
                    <tr>
                      <td>Version</td>
                      <td>
                        <Placeholder as={Card.Text} animation="glow">
                          <Placeholder xs={4}/>
                        </Placeholder>
                      </td>
                    </tr>
                    <tr>
                      <td>Network</td>
                      <td>
                        <Placeholder as={Card.Text} animation="glow">
                          <Placeholder xs={4}/>
                        </Placeholder>
                      </td>
                    </tr>
                    <tr>
                      <td>Block Height</td>
                      <td>
                        <Placeholder as={Card.Text} animation="glow">
                          <Placeholder xs={4}/>
                        </Placeholder>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Col>
            ) : ''}
          </Row>
        </Card.Body>
      </Card>
      <Card className='mt-3 text-white' bg="custom-card" style={!isMobile ? {width: "70%", margin: "0 auto"} : {}}>
      <Card.Body>
        <Card.Title className='center'>Transactions</Card.Title>
        <hr/>
        <Placeholder as={Card.Title} animation="glow">
          <Placeholder xs={12} size="lg"/>
        </Placeholder>
      </Card.Body>
    </Card>
    </>
  )
}
