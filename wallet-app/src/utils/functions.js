import axios from 'axios';
import * as Bip39 from 'bip39';
import CryptoJS from 'crypto-js';
import * as HDPrivateKey from 'bitcore-lib-nexa/lib/hdprivatekey'
import NexaAddress, { MoneyNetwork } from './NexaAddress';

export async function getBlockHeight() {
    var req = await axios.get(process.env.REACT_APP_API_URL + "/height")
    return req.data;
}

export async function checkBalanceAndTransactions(rAddrs, cAddrs, height) {
    var req = await axios.post(process.env.REACT_APP_API_URL + "/checkdata", { 
        receiveAddresses: rAddrs,
        changeAddresses: cAddrs,
        fromHeight: height
    });
    return req.data;
}

export async function fetchUtxos(rAddrs, cAddrs, toType, subFromAmount, manualFee, value) {
    var req = await axios.post(process.env.REACT_APP_API_URL + "/utxos", { 
        receiveAddresses: rAddrs,
        changeAddresses: cAddrs,
        type: toType,
        feeFromAmount: subFromAmount,
        explicitFee: manualFee,
        amount: value
    });
    return req.data;
}

export async function checkAddresses(rAddrs, cAddrs) {
    var req = await axios.post(process.env.REACT_APP_API_URL + "/scanaddresses", { 
        receiveAddresses: rAddrs,
        changeAddresses: cAddrs,
        fromHeight: 0
    });
    return req.data;
}

export async function broadcastTransaction(txHex) {
    var req = await axios.post(process.env.REACT_APP_API_URL + "/sendtx", { 
        hex: txHex
    });
    return req.data;
}


export function validatePassword(pw, pwConfirm) {
    if (pw !== pwConfirm) {
        return "Passwords are not equal."
    }

    if (pw.length < 8) {
        return "Must contain at least 8 or more characters."
    }

    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    var numbers = /[0-9]/g;

    if (!pw.match(lowerCaseLetters)) {
        return "Must contain a lower case letter."
    }

    if (!pw.match(upperCaseLetters)) {
        return "Must contain an upper case letter."
    }

    if (!pw.match(numbers)) {
        return "Must contain a number."
    }

    return "";
}

export function generateWords() {
    return Bip39.generateMnemonic(128, null, Bip39.wordlists.english);
}

export function isMnemonicValid(mnemonic) {
    return Bip39.validateMnemonic(mnemonic,  Bip39.wordlists.english);
}

export function getEncryptedSeed() {
    var wInfo = localStorage.getItem("wallet");
    if (wInfo !== null) {
        wInfo = JSON.parse(wInfo);
        if (wInfo.mnemonic) {
            return wInfo.mnemonic;
        }
    }
    return "";
}

export function decryptMnemonic(encSeed, password) {
    return CryptoJS.AES.decrypt(Buffer.from(encSeed, 'hex').toString(), CryptoJS.SHA512(password).toString()).toString(CryptoJS.enc.Utf8);
}

export function encryptAndStoreMnemonic(phrase, password) {
    var encMn = CryptoJS.AES.encrypt(phrase, password).toString();
    var encMnHex = Buffer.from(encMn).toString('hex');
    localStorage.setItem('wallet', JSON.stringify({mnemonic: encMnHex}));
}

/**
 * 
 * @param {HDPrivateKey} accountKey
 * @param {number} rIndex 
 * @param {number} cIndex 
 */
export function generateKeysAndAddresses(accountKey, fromRIndex, rIndex, fromCIndex, cIndex) {
    let receive = accountKey.deriveChild(0, false);
    let change = accountKey.deriveChild(1, false);
    let rKeys = [], cKeys = [];
    for (let index = fromRIndex; index < rIndex; index++) {
        let k = receive.deriveChild(index, false);
        let addr = NexaAddress.p2pkt(MoneyNetwork.NEXA, k.publicKey);
        rKeys.push({key: k, address: addr});
    }
    for (let index = fromCIndex; index < cIndex; index++) {
        let k = change.deriveChild(index, false);
        let addr = NexaAddress.p2pkt(MoneyNetwork.NEXA, k.publicKey);
        cKeys.push({key: k, address: addr});
    }
    return {receiveKeys: rKeys, changeKeys: cKeys};
}