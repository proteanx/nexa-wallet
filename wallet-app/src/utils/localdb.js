import Dexie from 'dexie';

export const localdb = new Dexie("nexa");
localdb.version(1).stores({
  transactions: 'txIdem, time'
});

export async function clearCachedWallet() {
  localStorage.removeItem("tx-state");
  localStorage.removeItem("wallet-idx");
  return await localdb.transactions.clear();
}

export async function addLocalTransactions(txs, pending, confirmed, height, total) {
  setTransactionsState({pending: pending, confirmed: confirmed, height: height, total: total});
  txs.forEach(async tx => {
      try {
          await localdb.transactions.put(tx, tx.txIdem);
      } catch (error) {
          console.log(error);
      }
  });
}

export async function addLocalTransaction(tx) {
    try {
        await localdb.transactions.put(tx, tx.txIdem);
    } catch (error) {
        console.log(error);
    }
}

export async function getLocalTransactions() {
  return await localdb.transactions.toArray();
}

export async function getPageLocalTransactions(pageNum, pageSize) {
  return await localdb.transactions.orderBy('time').reverse().offset((pageNum-1)*pageSize).limit(pageSize).toArray();;
}

export async function countLocalTransactions() {
  return await localdb.transactions.count();
}

export function setTransactionsState(state) {
  localStorage.setItem("tx-state", JSON.stringify(state));
}

export function getTransactionsState() {
  var state = localStorage.getItem("tx-state");
  return state !== null ? JSON.parse(state) : {pending: 0, confirmed: 0, height: 0, total: 0}; 
}

export function setAddressesIndexes(aIndexes) {
  localStorage.setItem("wallet-idx", JSON.stringify(aIndexes));
}

export function getAddressesIndexes() {
  var idx = localStorage.getItem("wallet-idx");
  return idx !== null ? JSON.parse(idx) : {rIndex: 0, cIndex: 0};
}

export function setLock(lock) {
  if (localStorage.getItem(lock) == null) {
    localStorage.setItem(lock, "true");
    return true;
  }
  return false;
}

export function removeLock(lock) {
  localStorage.removeItem(lock);
}

export function setLastCheck() {
  var time = Math.floor(Date.now() / 1000);
  localStorage.setItem("last-check", time);
}

export function getLastCheck() {
  return localStorage.getItem("last-check");
}