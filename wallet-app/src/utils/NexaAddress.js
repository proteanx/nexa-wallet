import NexaAddr from 'nexaaddrjs';
import Bitcore from 'bitcore-lib-nexa'
import BufferReader from 'bitcore-lib-nexa/lib/encoding/bufferreader';

export const MoneyNetwork = {
    NEXA: 'nexa',
    TESTNET: 'nexatest'
};

export const Type = {
    P2PKH: 'P2PKH',
    P2PKT: 'TEMPLATE'
};

export default class NexaAddress {
    static p2pkh(network, publicKey) {
        return NexaAddr.encode(network, Type.P2PKH, Bitcore.crypto.Hash.sha256ripemd160(publicKey.toBuffer()));
    }

    static p2pkt(network, publicKey) {
        var scriptPushPubKey = Bitcore.Script.empty().add(publicKey.toBuffer());
        var hash160scriptPushPubKey = Bitcore.crypto.Hash.sha256ripemd160(scriptPushPubKey.toBuffer());
        var scriptTemplate = Bitcore.Script.empty().add(Bitcore.Opcode.OP_FALSE).add(Bitcore.Opcode.OP_TRUE).add(hash160scriptPushPubKey).toBuffer();
      
        var finalScript = Bitcore.Script.empty().add(scriptTemplate).toBuffer();
      
        return NexaAddr.encode(network, Type.P2PKT, finalScript);
    }

    static getAddressScript(address) {
        var addr = NexaAddr.decode(address);
        if (addr.type === 'P2PKH') {
            return Bitcore.Script.empty()
                .add(Bitcore.Opcode.OP_DUP)
                .add(Bitcore.Opcode.OP_HASH160)
                .add(addr.hash)
                .add(Bitcore.Opcode.OP_EQUALVERIFY)
                .add(Bitcore.Opcode.OP_CHECKSIG).toBuffer();
        }

        if (addr.type === 'TEMPLATE') {
            return new BufferReader(Buffer.from(addr.hash)).readVarLengthBuffer();
        }

        return "invalid";
    }

    static getAddressOutputType(address) {
        var addr = NexaAddr.decode(address);
        if (addr.type === 'P2PKH') {
            return 0;
        }

        if (addr.type === 'TEMPLATE') {
            return 1;
        }

        return "invalid";
    }
}
