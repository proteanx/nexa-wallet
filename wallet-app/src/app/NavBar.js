import React, { useState } from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import nex from '../img/nex.svg'
import Donation from './Donation';
import Button from 'react-bootstrap/Button';

export default function NavBar({ showLogout }) {
  const [width] = useState(window.innerWidth);
  const isMobile = (width <= 768);

  const reload = () => {
    window.location.reload();
  }

  return (
    <Navbar bg="dark" expand="lg" variant="dark" className='px-3'>
      <Container>
        <Navbar.Brand href='/'>
            <img alt="Nexa" src={nex} className="header-image"/>
            Nexa Wallet
        </Navbar.Brand>
        <Nav.Item className="ms-auto">
          {showLogout && <Button className='me-1' variant="warning" size="sm" onClick={reload}><i className="fa-brands fa-expeditedssl fa-lg"/>{isMobile || ' Lock'}</Button>}
          <Donation/>
        </Nav.Item>
      </Container>
    </Navbar>
  )
}
