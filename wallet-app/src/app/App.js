import React, { useState } from 'react';
import Container from 'react-bootstrap/esm/Container';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import './App.css';
import Footer from './Footer';
import NavBar from './NavBar';
import Wallet from '../wallet/Wallet';
import CreateWallet from '../wallet/create/CreateWallet';
import OpenWallet from '../wallet/OpenWallet';
import RecoverWallet from '../wallet/recover/RecoverWallet';
import { getEncryptedSeed } from '../utils/functions';

function App() {
  const [width] = useState(window.innerWidth);
  const isMobile = (width <= 768);

  const [decSeed, setDecSeed] = useState('');
  const [create, setCreate] = useState(false);
  const [restore, setRestore] = useState(false);

  var encSeed = getEncryptedSeed();

  const handleCreate = () => setCreate(true);
  const cancelCreate = () => setCreate(false);
  const handleRestore = () => setRestore(true);
  const cancelRestore = () => setRestore(false);

  let content = <Card.Body>
                  <div className='mb-4'>
                    Welcome to Nexa wallet
                  </div>
                  <div>
                    <Button variant="outline-warning" className='m-2' onClick={handleCreate}>Create Wallet</Button>
                    <Button variant="outline-warning" className='m-2' onClick={handleRestore}>Recover Wallet</Button>
                  </div>
                </Card.Body>
                

  if (create) {
    content = <CreateWallet cancelCreate={cancelCreate} setDecSeed={setDecSeed}></CreateWallet>
  }

  if (restore) {
    content = <RecoverWallet cancelRecover={cancelRestore} setDecSeed={setDecSeed}></RecoverWallet>
  }

  if (decSeed === '' && encSeed !== '') {
    content = <OpenWallet encSeed={encSeed} setDecSeed={setDecSeed}></OpenWallet>
  }
  
  return (
    <>
    <NavBar showLogout={decSeed !== ''}/>
    <div className='pb-4 pt-3 pt-md-4' style={{backgroundColor: "#3C2F48", minHeight: "80%"}}>
      <Container>
        { decSeed !== '' ? (
          <Wallet mnemonic={decSeed}/>
        ) : (
          <Card style={!isMobile ? {width: "40%"} : {}} bg="custom-card" className='text-white center'>
            {content}
          </Card>
        ) }
      </Container>
    </div>
    <Footer/>
    </>
  );
}

export default App;
