import React from 'react'
import Donation from './Donation'

export default function Footer() {
  return (
    <footer className='p-3 border-top border-secondary'>
        Copyright (c) 2023 vgrunner @ Nexa Community Developer. All Rights Reserved.<br/>
    </footer>
  )
}
