import React, { useState } from 'react'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function Donation() {
  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  return (
    <>
      <Button variant="warning" size="sm" onClick={handleShow}><i className="fa fa-heart"/> Donate</Button>

      <Modal contentClassName='bg-dark text-white text-dev' show={show} onHide={handleClose} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title>Support Developer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p className='text-info text-dev'>
            This Wallet developed and maintained by vgrunner, a Nexa community developer.
            <br/>Please consider a donation to help with the costs of keep maintaining and developing the wallet,
            as well as buy me a coffee and show me some love for my work. :)
          </p>
          <h6 className='mt-4 text-dev'>Donate by address</h6>
          <div className='text-monospace url'>
            NEXA: <a rel="noreferrer" target={"_blank"} href='https://explorer.nexa.org/address/nexa:nqtsq5g5hv503nxqagm64r54e9eryg3zj0y3wlgwfxgqagtl'>nexa:nqtsq5g5hv503nxqagm64r54e9eryg3zj0y3wlgwfxgqagtl</a>
          </div>
          <div className='text-monospace url'>
            BCH: <a rel="noreferrer" target={"_blank"} href='https://explorer.bitcoinunlimited.info/address/bitcoincash:qzveujdh5qvxq8zznl7czpvkvrzn9an66gsyj7nenq'>bitcoincash:qzveujdh5qvxq8zznl7czpvkvrzn9an66gsyj7nenq</a>
          </div>
          <div className='text-monospace url'>
            BTC: <a rel="noreferrer" target={"_blank"} href='https://blockstream.info/address/bc1q87lhx8eg93cj5s0hxgl6ns9l0cgj5j4yrn909a'>bc1q87lhx8eg93cj5s0hxgl6ns9l0cgj5j4yrn909a</a>
          </div>
          <div className='text-monospace url'>
            ETH: <a rel="noreferrer" target={"_blank"} href='https://etherscan.io/address/0xFA25dbcB0B892a95461870C51e69876aD38BF60F'>0xFA25dbcB0B892a95461870C51e69876aD38BF60F</a>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
