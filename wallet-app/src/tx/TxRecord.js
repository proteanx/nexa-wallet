import bigDecimal, { RoundingModes } from 'js-big-decimal';
import React from 'react'

export default function TxRecord({record}) {
  const date = new Date(record.time * 1000).toLocaleDateString();
  const time = new Date(record.time * 1000).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit", hour12: false });
  
  var amount = new bigDecimal(record.value).round(2, RoundingModes.HALF_DOWN).getPrettyValue();
  var incoming = record.value > 0;
  var type = record.address.includes('yourself') ? <i className="fa fa-repeat"/> : (incoming ? <i className="fa fa-download"/> : <i className="fa fa-upload"/>)

  return (
    <tr className='tr-border'>
      <td className='center va td-min' title={record.confirmed ? 'confirmed' : 'unconfirmed'}>{record.confirmed ? <i className="mx-1 fa fa-check nx"/> : <i className="mx-1 fa-regular fa-clock nx"/>}</td>
      <td className='center va'>{type}</td>
      <td className='td-min'>
        {date + " " + time}
        <br/>
        {record.txIdem}
        <br/>
        {(incoming ? '' : "Sent to: ") + record.address}
      </td>
      <td className='va center'>
        <span className={incoming ? 'incoming' : 'outgoing'}>
          {incoming ? '+' : ''}{amount} NEX
        </span>
      </td>
    </tr>
  )
}