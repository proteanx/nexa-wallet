import React, { useEffect, useState } from 'react'
import TxRecord from './TxRecord'
import Table from 'react-bootstrap/Table';
import Card from 'react-bootstrap/Card';
import Pagination from 'react-bootstrap/Pagination';
import { countLocalTransactions, getPageLocalTransactions } from '../utils/localdb';
import { useLiveQuery } from "dexie-react-hooks";

export default function TxList({ refresh }) {
  const [width] = useState(window.innerWidth);
  const isMobile = (width <= 768);

  const [tmpVal, setTmpVal] = useState(0);
  const [pageNum, setPageNum] = useState(1);
  const [pageSize] = useState(10);

  useEffect(() => {
    const myInterval = setInterval(() => {
      setTmpVal((prevVal) => prevVal + 1);
    }, 30 * 1000);

    return () => clearInterval(myInterval);
  }, []);

  useEffect(() => {
    if (tmpVal != 0) {
      refresh();
    }
  }, [tmpVal]);

  const txCount = useLiveQuery(() => countLocalTransactions());
  const transactions = useLiveQuery(() => getPageLocalTransactions(pageNum, pageSize), [pageNum, pageSize]);

  let pagination = "";
  let paginationItems = [];
  if (txCount) {
    var pages = Math.ceil(txCount / pageSize);
    if (pages > 1) {
      let start = 1;
      if (pageNum - 2 > 3) {
        paginationItems.push(
          <Pagination.Item key={1} onClick={() => setPageNum(1)}>
            {1}
          </Pagination.Item>
        )
        paginationItems.push(<Pagination.Ellipsis/>);
        start = pageNum - 2;
      } 

      for (let i = start; i <= pageNum + 2 && i < pages; i++) {
        paginationItems.push(
          <Pagination.Item key={i} active={i === pageNum} onClick={() => setPageNum(i)}>
            {i}
          </Pagination.Item>
        )
        start = i+1;
      }

      if (pages - pageNum > 4) {
        paginationItems.push(<Pagination.Ellipsis/>);
        start = pages;
      }

      for (let i = start; i <= pages; i++) {
        paginationItems.push(
          <Pagination.Item key={i} active={i === pageNum} onClick={() => setPageNum(i)}>
            {i}
          </Pagination.Item>
        )
      }

      pagination = <Pagination className='d-flex justify-content-center'>
                    <Pagination.First disabled={pageNum == 1} onClick={() => setPageNum(1)}/>
                    {paginationItems}
                    <Pagination.Last disabled={pageNum == pages} onClick={() => setPageNum(pages)}/>
                  </Pagination>
    }
  }

  return (
    <Card className='mt-3 text-white' bg="custom-card" style={!isMobile ? {width: "70%", margin: "0 auto"} : {}}>
      <Card.Body>
        <Card.Title className='center'>Transactions ({txCount})</Card.Title>
        <hr/>
        <Table borderless responsive striped className='text-white'>
          <tbody>
            { transactions?.map((tx, i) => <TxRecord key={i} record={tx}/>) }
          </tbody>
        </Table>
        {pagination}
      </Card.Body>
    </Card>
  )
}
