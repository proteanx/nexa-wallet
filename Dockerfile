FROM node:lts-alpine3.17 as builder

ENV NODE_ENV production

ARG REACT_APP_API_URL
ENV REACT_APP_API_URL $REACT_APP_API_URL

WORKDIR /app
COPY ./wallet-app/ .
RUN npm i && npm i ./bitcore-lib-nexa
RUN npm run build

FROM nginx:stable-alpine as production

COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

RUN apk --no-cache add curl && rm -f /var/cache/apk/*

ARG DOCKERIZE_VERSION=v0.16.5
ARG DOCKERIZE_URL=https://github.com/powerman/dockerize/releases/download

RUN curl -sL $DOCKERIZE_URL/$DOCKERIZE_VERSION/dockerize-linux-x86_64 > /usr/local/bin/dockerize \
    && chmod +x /usr/local/bin/dockerize

EXPOSE 3010

CMD ["dockerize", "-wait", "tcp://walletapi:10102", "nginx", "-g", "daemon off;"]